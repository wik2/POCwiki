[[_TOC_]]
# POCwiki

This repo serves as a POC for the wiki process I am trying to create.

I am no creating a few wiki pages to test compatibility.

# Main goals

The following are the goals for this project:
- automated build process that will push updates to this repo to one of the git wiki
- ability to do "Blame" on portions of the wiki so you can reach out to the correct people if you need more info
- pull requests on wikis.
- Different groups able to approve pull requests for different sections
- link to different pages [like this](/ExamplePage.md) or [like this](/BigProject/BigProjectPage1.md)
- testing that changes here will be automatically mirrored in the wiki